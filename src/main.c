#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <error.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv){
	if(argc<3){
		printf("Rutas invalidas");
		exit(-1);
	}

	int fuente;
	int destino;
	umask(0);
	fuente = open(argv[1],O_RDONLY,0666);
	destino= open(argv[2],O_WRONLY|O_CREAT|O_TRUNC|O_APPEND,0666);
	if(fuente<0){
		perror("No se puede abrir los archivos fuente");
		exit(-1);
	}

	if(destino<0){
		perror("No se puede abrir los archivos fuente");
		exit(-1);
	}

	int byteCopiados=0;
	int byteLeido=0;
	char *b = malloc(sizeof(char)*10000);

	do{
		byteLeido= read(fuente,b,10000);
		if(byteLeido<0){
			perror("Fallo al leer el archivo");
			exit(-1);
		}

		if(byteLeido>0){
			int escrito= write(destino,b,byteLeido);
			byteCopiados= byteCopiados+byteLeido;
			if(escrito<0){
				perror("Fallo en escritura");
				exit(-1);
			}
			memset(b,0,10000);
		}
	}while(byteLeido==10000);

	printf("%d bytes copiados \n",byteCopiados);
	free(b);
	close(fuente);
	close(destino);
	
	return 0;
}
