
bin/pscopy: obj/main.o
	mkdir -p bin/
	gcc obj/main.o -o bin/pscopy
	 

obj/main.o: src/main.c
	mkdir -p obj/
	gcc -Wall -c src/main.c -o obj/main.o
	
.PHONY: clean
clean:
	rm obj/* bin/*	

